const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		require: [true, "Course is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true 	
	},
	createdOn: {
		type: Date,
		// date and time course was created
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "User Id is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]

});

module.exports = mongoose.model("Course", courseSchema);
// Course will become "Courses" collection in database